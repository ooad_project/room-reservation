import api from './api'

export function getRequestLists () {
  return api.get('/requests')
}
