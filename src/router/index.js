import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path: '/product',
    name: 'Product',
    component: () => import(/* webpackChunkName: "about" */ '../views/Product.vue')
  },
  {
    path: '/calendar',
    name: 'Calendar',
    component: () => import(/* webpackChunkName: "about" */ '../views/Calendar.vue')
  },
  {
    path: '/mainpage',
    name: 'Main',
    component: () => import(/* webpackChunkName: "about" */ '../views/MainPage.vue')
  },
  {
    path: '/selectbuilding',
    name: 'SelectBuilding',
    component: () => import(/* webpackChunkName: "about" */ '../views/SelectBuilding.vue')
  },
  {
    path: '/statusbooking',
    name: 'StatusBooking',
    component: () => import(/* webpackChunkName: "about" */ '../views/StatusBooking.vue')
  },
  {
    path: '/selectroom/:id',
    name: 'SelectRoom',
    component: () => import(/* webpackChunkName: "about" */ '../views/SelectRoom.vue')
  },
  {
    path: '/detailsroom/:id',
    name: 'DetailsRoom',
    component: () => import(/* webpackChunkName: "about" */ '../views/DetailsRoom.vue')
  },
  {
    path: '/bookingroompage/:id',
    name: 'BookingRoomPage',
    component: () => import(/* webpackChunkName: "about" */ '../views/BookingRoomPage.vue')
  },
  {
    path: '/requestlist',
    name: 'RequestList',
    component: () => import(/* webpackChunkName: "about" */ '../views/RequestList.vue')
  },
  {
    path: '/detailrequestlist/:id',
    name: 'DetailRequestList',
    component: () => import(/* webpackChunkName: "about" */ '../views/DetailRequestList.vue')
  },
  {
    path: '/buildingmanagement',
    name: 'BuildingManagement',
    component: () => import(/* webpackChunkName: "about" */ '../views/BuildingManagement.vue')
  },
  {
    path: '/adminmanagement',
    name: 'AdminManagement',
    component: () => import(/* webpackChunkName: "about" */ '../views/AdminManagement.vue')
  },
  {
    path: '/institutionmanagement',
    name: 'InstitutionManagement',
    component: () => import(/* webpackChunkName: "about" */ '../views/InstitutionManagement.vue')
  },
  {
    path: '/reportusageroomsmain',
    name: 'ReportUsageRoomsMain',
    component: () => import(/* webpackChunkName: "about" */ '../views/ReportUsageRoomsMain.vue')
  },
  {
    path: '/membermanagement',
    name: 'MemberManagement',
    component: () => import(/* webpackChunkName: "about" */ '../views/MemberManagement.vue')
  },
  {
    path: '/internalmanagement',
    name: 'InternalManagement',
    component: () => import(/* webpackChunkName: "about" */ '../views/InternalManagement.vue')
  },
  {
    path: '/roommanagement',
    name: 'RoomManagement',
    component: () => import(/* webpackChunkName: "about" */ '../views/RoomManagement.vue')
  },
  {
    path: '/approvedordermanagement',
    name: 'ApprovedOrderManagement',
    component: () => import(/* webpackChunkName: "about" */ '../views/ApprovedOrderManagement.vue')
  },
  {
    path: '/detailreport/:id',
    name: 'DetailReport',
    component: () => import(/* webpackChunkName: "about" */ '../views/DetailReport.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
